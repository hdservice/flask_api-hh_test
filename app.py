import requests
from flask import Flask, render_template, request

from config import LINK

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        find = str(request.form.get('find')).strip()

        response = requests.get(f'{LINK}/employers?text={find}').json()

        if len(response['items']) == 0:
            return render_template('index.html', title=f'Поиск {find} среди компаний не вернул результатов')
        else:
            return render_template('index.html', title=f'Поиск {find} среди компаний', company=response['items'])
    else:
        return render_template('index.html', title='Поиск компаний - работодателей')


@app.route('/company/<int:id>')
def company(id):
    response = requests.get(f'{LINK}/employers/{id}').json()

    vacancies = requests.get(response['vacancies_url']).json()['items']

    return render_template('company.html',
                           title=f"Просмотр компании {response['name']}",
                           description=response['description'],
                           site_url=response['site_url'],
                           area=response['area']['name'],
                           open_vacancies=response['open_vacancies'],
                           vacancies=vacancies)


if __name__ == '__main__':
    app.run(debug=True)
